from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm


def todo_list_list(request):
    lists = TodoList.objects.all()
    context = {
        "todo_lists": lists,
    }
    return render(request, "todo_list_list.html", context)


def todo_list_detail(request, id):
    item = get_object_or_404(TodoList, id=id)
    context = {
        "todo_items": item,
    }
    return render(request, "todo_list_detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {
        "form": form,
    }
    return render(request, "todo_list_create", context)
